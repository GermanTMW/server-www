<?php

	/*
	** - Copying and Changing is allowed! -
	** - Pleace dont remove this Comments -
	** - Author : Mike Wollmann aka. jak1 -
	*/

	include("src/config.php");
	include("src/GitLab.php");
	$counter = 0;
	$cachefile = "./src/.cache";
	

	cache($CONFIG,$cachefile);


	function getOnlineUsersDB($CONFIG){
		global $counter;

		$table = array("<tr><td>","</td></tr>");
		$con = mysqli_connect(	$CONFIG["mysql-hostname"], $CONFIG["mysql-username"], $CONFIG["mysql-password"], 
								$CONFIG["mysql-prefixes"].$CONFIG["mysql-database"]);
		
		if (mysqli_connect_errno())
		{
			die("Failed to connect to MySQL: " . mysqli_connect_error());	
		}
		else
		{	
			if ($CONFIG["use-tableformating"] == 0)
			{
				$table[0] = "";
				$table[1] = "<br>";
			}
			else
			{
				$TMP = "<table border='1' cellspacing='1'>";
			}
			$chars = mysqli_query($con, "select account_id, name from `char` where online <> 0");
			while($row = mysqli_fetch_assoc($chars))
			{
				
				$accs = mysqli_query($con, "SELECT group_id FROM `login` WHERE account_id =" . $row['account_id'] . " LIMIT 1");
				$row2 = mysqli_fetch_assoc($accs);
				$prefix = "";
				$suffix = "";
				if ( ( $row2['group_id'] >= $CONFIG['group_id>show(GM)']) && ( $row2['group_id'] < 99 ) )
				{
					$prefix = "<font color='green'><b>";
					$suffix = "(GM)</b></font>";
				}
				else if ($row2['group_id'] == 99)
				{
					$prefix = "<font color='red'><b>";
					$suffix = "(Admin)</b></font>";
				}
				$counter +=1;
				$TMP .= $table[0] . $prefix . $row['name'] . " " . $suffix . $table[1];
			}
			if ($CONFIG["use-tableformating"] == 1)
			{
				$TMP.="</table>";
			}
		}
		return $TMP;
	}

	function getdatetime()
	{
		return date('d.m.y - H:i:s', gmdate('U'));
	}
	
	function cache($CONFIG, $cachefile)
	{
		if ( time() >= (filectime($cachefile) + 20) )
		{
			$users = getOnlineUsersDB($CONFIG);
			global $counter;
			$cache ="<!DOCTYPE html><html><head>"
				."<META http-equiv='Refresh' content='10'>"
				."<title>" . $CONFIG['ServerName'] . "'s Onlinelist</title>"
				."</head><body><center>"
				."<h2>" . $counter . " Online Players on" . $CONFIG['ServerName'] . " (".getdatetime().")</h2>"
				. $users
				."</center></body></html>";
			$cf = fopen($cachefile, "w") or die("Unable to open file!");
			fwrite($cf, $cache);
			fclose($cf);
			include($cachefile);
		}
		else
		{
			include($cachefile);
		}

	}


?>
